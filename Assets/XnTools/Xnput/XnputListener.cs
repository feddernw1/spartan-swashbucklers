using UnityEngine;
using System.Collections.Generic;
using System.Text;
using UnityEngine.InputSystem;
using UnityEngine.PlayerLoop;

namespace XnTools {
    public enum eXnputProgressType { lateUpdate, fixedUpdate, manual }
    
    
    [System.Serializable]
    public class XnputListener {

        public InfoProperty info = new InfoProperty( "XnputListener Instructions",
            "This object will automatically register to listen for all Xnput messages. " +
            "It is able to Progress (née Clear) on a separate pace from the Xnput Singleton, " +
            "which is very useful in situations like Spartan Swashbucklers where the Human " +
            "captain doesn't get updates every frame and therefore was missing button presses.\n" +
            "<b>RegisterListeners() and UnRegisterListeners() MUST be called manually!</b>",
            true, false );
        
        public eXnputProgressType progressOn = eXnputProgressType.lateUpdate;
        
        private Dictionary<Xnput.eButton, ButtonState> buttonDict = new Dictionary<Xnput.eButton, ButtonState>();
        // public ButtonState up, down, left, right, a, b, start, select;
        [ReadOnly]
        public string             buttons;
        [ReadOnly]
        public Vector2            moveRaw;

        public void RegisterListeners() {
            Xnput.buttonEvent += OnButtonEvent;
            Xnput.moveEvent += OnMoveEvent;
            Xnput.lateUpdateEvent += LateUpdateEvent;
            Xnput.fixedUpdateEvent += FixedUpdateEvent;
        }

        public void UnRegisterListeners() {
            Xnput.buttonEvent -= OnButtonEvent;
            Xnput.moveEvent -= OnMoveEvent;
            Xnput.lateUpdateEvent -= LateUpdateEvent;
            Xnput.fixedUpdateEvent -= FixedUpdateEvent;
        }

        // private void InitButtonDict() {
        //     buttonDict = new Dictionary<Xnput.eButton, ButtonState> {
        //         { Xnput.eButton.up, up },
        //         { Xnput.eButton.down, down },
        //         { Xnput.eButton.left, left },
        //         { Xnput.eButton.right, right },
        //         { Xnput.eButton.a, a },
        //         { Xnput.eButton.b, b },
        //         { Xnput.eButton.start, select },
        //         { Xnput.eButton.select, start }
        //     };
        // }
        
        public bool GetButton( Xnput.eButton eB ) {
            if ( buttonDict.ContainsKey( eB ) ) {
                return buttonDict[eB];
            } else {
                return false;
            }
        }

        public bool GetButtonDown( Xnput.eButton eB ) {
            if ( buttonDict.ContainsKey( eB ) ) {
                return buttonDict[eB].down;
            } else {
                return false;
            }
        }

        public bool GetButtonUp( Xnput.eButton eB ) {
            if ( buttonDict.ContainsKey( eB ) ) {
                return buttonDict[eB].up;
            } else {
                return false;
            }
        }
        
        public float hRaw {
            get { return moveRaw.x; }
        }

        public float vRaw {
            get { return moveRaw.y; }
        }
        
        public float GetAxisRaw( Xnput.eAxis axis ) {
            if ( axis == Xnput.eAxis.horizontal ) return hRaw;
            if ( axis == Xnput.eAxis.vertical ) return vRaw;
            // Debug.LogError( $"Xnput does not have an axis named \"{axis}\"." );
            return 0;
        }

        private System.Text.StringBuilder buttonSB = new StringBuilder();
        void UpdateButtonStatusString() {
            buttonSB.Clear();
            foreach ( Xnput.eButton buttonID in Xnput.BUTTON_STRINGS.Keys ) {
                buttonSB.Append( Xnput.BUTTON_STRINGS[buttonID] );
                //buttonSB.Append( ":" );
                if ( buttonDict.ContainsKey( buttonID ) ) {
                    buttonSB.Append( buttonDict[buttonID].Char );
                } else {
                    buttonSB.Append( ButtonState.CHAR_DEFAULT );
                }
                buttonSB.Append( " " );
            }
            buttons = buttonSB.ToString();
        }

        /// <summary>
        /// <para>Progress should be called based on the progressOn value to cause the states to progress,
        /// for example Down > Held and Up > Free.</para>
        /// <para> Schedule for calling based on the progressOn value:</para>
        /// <para> - lateUpdate: Automatically called on LateUpdate() [DEFAULT]</para>
        /// <para> - fixedUpdate: Automatically called on FixedUpdate()</para>
        /// <para> - manual: Must be called manually</para> 
        /// </summary>
        public void Progress() {
            foreach ( ButtonState bs in buttonDict.Values ) {
                bs.Progress();
            }
            UpdateButtonStatusString();
        }

        private void OnButtonEvent( Xnput.eButton buttonID, InputValue value ) {
            if ( !buttonDict.ContainsKey( buttonID ) ) {
                buttonDict.Add( buttonID, new ButtonState() );
            } 
            buttonDict[buttonID].Set( value.isPressed );
            UpdateButtonStatusString();
        }

        private void OnMoveEvent( InputValue value ) {
            moveRaw = value.Get<Vector2>();
        }

        private void LateUpdateEvent() {
            if ( progressOn == eXnputProgressType.lateUpdate ) Progress();
        }

        private void FixedUpdateEvent() {
            if ( progressOn == eXnputProgressType.fixedUpdate ) Progress();
        }
    }
}
