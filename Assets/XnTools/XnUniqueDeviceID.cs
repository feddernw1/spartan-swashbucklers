﻿using UnityEngine;
using System.Collections.Generic;

namespace XnTools {
    static public class XnUniqueDeviceID {
        const string prefsKey = "XnUniqueDeviceID";

        static string _ID = "";

        static public string ID {
            get {
                if ( _ID != "" ) return _ID;
                return GetUDID();
            }
        }

        /// <summary>
        /// Gets a UDID for this device and saves it to PlayerPrefs (if saveToPrefs == true).
        /// Attempts to use Unity's SystemInfo.deviceUniqueIdentifier but also has fallback.
        /// </summary>
        /// <param name="saveToPrefs">If true, the UDID is saved to PlayerPrefs.</param>
        /// <returns></returns>
        static public string GetUDID( bool saveToPrefs = true ) {
            // Check to see if it is already in the PlayerPrefs
            if ( PlayerPrefs.HasKey( prefsKey ) ) {
                _ID = PlayerPrefs.GetString( prefsKey );
                return _ID;
            }

            // Attempt to use Unity's solution
            string id = SystemInfo.deviceUniqueIdentifier;
            if ( id == SystemInfo.unsupportedIdentifier ) {
                // Generate our own
                id = GenerateUDID();
            }
            if ( saveToPrefs ) PlayerPrefs.SetString( prefsKey, id );
            _ID = id;
            return _ID;
        }

        /// <summary>
        /// Fallback for when Unity's SystemInfo.deviceUniqueIdentifier fails.
        /// Includes much more system information while nearly guaranteeing uniqueness.
        /// </summary>
        /// <returns>Example: Desktop|OSXEditor|MacBookPro16,1|ExNinja MacBook Pro (1601)|Intel(R) Core(TM) i9-9980HK CPU @ 2.40GHz|20230117051423</returns>
        static public string GenerateUDID() {
            // Generate our own
            List<string> bits = new List<string>();
            bits.Add( SystemInfo.deviceType.ToString() );
            bits.Add( Application.platform.ToString() );
            bits.Add( SystemInfo.deviceModel );
            bits.Add( SystemInfo.deviceName );
            if ( SystemInfo.processorType != SystemInfo.unsupportedIdentifier ) bits.Add( SystemInfo.processorType );
            bits.Add( GenerateDateTimeString( eDateResolution.milliseconds) );
            string id = string.Join( "|", bits );
            return id;
        }

        /// <summary>
        /// Generates a uint to represent Date as a series of numbers YYYYMMDD.
        /// </summary>
        /// <param name="dateRes"></param>
        /// <returns>Example: 20230117</returns>
        public static uint GenerateDateUint() {
            System.DateTime dt = System.DateTime.UtcNow;
            uint u = (uint) ( dt.Year * 10000 + dt.Month * 100 + dt.Day );
            return u;
        }

        public enum eDateResolution { dateOnly, minutes, seconds, milliseconds };

        /// <summary>
        /// Generates a ulong to represent DateTime as a series of numbers YYYYMMDDhhmmss...
        /// </summary>
        /// <param name="dateRes"></param>
        /// <returns>Examples at various dateRes levels: 20230117, 202301170514, 20230117051423, 20230117051423034</returns>
        public static ulong GenerateDateTimeUlong( eDateResolution dateRes = eDateResolution.dateOnly ) {
            System.DateTime dt = System.DateTime.UtcNow;
            // Makes it into a simple number that will be different every day of every year
            long l = dt.Year * 10000 + dt.Month * 100 + dt.Day;
            if ( dateRes >= eDateResolution.minutes ) {
                l = l * 10000 + dt.Hour * 100 + dt.Minute;
            }
            if ( dateRes >= eDateResolution.seconds ) {
                l = l * 100 + dt.Second;
            }
            if ( dateRes >= eDateResolution.milliseconds ) {
                l = l * 1000 + dt.Millisecond;
            }
            return (ulong) l;
        }

        /// <summary>
        /// Generates a string to represent DateTime as a series of numbers YYYYMMDDhhmmss...
        /// Note: This works by calling GenerateDateTimeUlong().ToString()
        /// </summary>
        /// <param name="dateRes"></param>
        /// <returns>Examples at various dateRes levels: 20230117, 202301170514, 20230117051423, 20230117051423034</returns>
        public static string GenerateDateTimeString(eDateResolution dateRes = eDateResolution.dateOnly) {
            ulong ul = GenerateDateTimeUlong( dateRes );

            // The code here demonstrates how to generate the same string without using GenerateDateTimeUlong();
            //System.DateTime dt = System.DateTime.UtcNow;
            //// Makes it into a simple number that will be different every day of every year
            //string s = $"{dt.Year:0000}{dt.Month:00}{dt.Day:00}";
            //if ( dateRes >= eDateResolution.minutes ) {
            //    s = $"{s}{dt.Hour:00}{dt.Minute:00}";
            //}
            //if ( dateRes >= eDateResolution.seconds ) {
            //    s = $"{s}{dt.Second:00}";
            //}
            //if ( dateRes >= eDateResolution.milliseconds ) {
            //    s = $"{s}{dt.Millisecond:000}";
            //}
            //return s;

            return ul.ToString();
        }


        public static void TEST() {
            XnUtils.TEST_DateTimeFunctions();

            Debug.Log( $"GET_UDID(false):\n{GetUDID( false )}" );

            Debug.Log( $"GENERATE_UDID():\n{GenerateUDID()}" );
        }
    }
}