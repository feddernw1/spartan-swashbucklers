using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using NaughtyAttributes;
using System;

namespace XnTools.XnAI {
    public interface XnAI_I_Behavior {
        string[] GetPublicMethods();
    }

    // public class ButtonAttribute : SpecialCaseDrawerAttribute
    [AttributeUsage( AttributeTargets.Method, AllowMultiple = false, Inherited = true )]
    public class SelectorSequenceAttribute : System.Attribute { }

    [AttributeUsage( AttributeTargets.Method, AllowMultiple = false, Inherited = true )]
    public class SelectorUtilityAttribute : System.Attribute { }

    [AttributeUsage( AttributeTargets.Method, AllowMultiple = false, Inherited = true )]
    public class SelectorParallelAttribute : System.Attribute { }

    [AttributeUsage( AttributeTargets.Method, AllowMultiple = false, Inherited = true )]
    public class BehaviorAttribute : System.Attribute { }

    
    
}