using UnityEngine;

namespace XnTools.XnAI {
    // public class XnAI : MonoBehaviour {
    //     public AITree aiTree;
    //
    //     protected virtual void Reset() {
    //         aiTree = new AITree( this );
    //     }
    //
    //     protected virtual void OnValidate() {
    //         aiTree.SetAITreeAndParent();
    //     }
    // }


    public interface iXnAI_SO {
        public iAINode selectedNode { get; set; }
        public XnLog   personalLog  { get; }
        public void UpdateAITreeStatus( string status );
    }
}