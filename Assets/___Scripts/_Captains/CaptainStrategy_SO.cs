using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using XnTools;
using XnTools.XnAI;


// [CreateAssetMenu( fileName = "Captain Strategy - Player",
//     menuName = "ScriptableObjects/Captain Strategy - Player", order = 1 )]
public abstract class CaptainStrategy_SO : ScriptableObject {
    [XnTools.ReadOnly]
    public string captainName = "NOT SET!";
    protected List<Ship.Command> commands = new List<Ship.Command>();

    private float moneyTotal;
    private float moneyOnShip;
    private float killsTotal;
    private float killsPVP;
    private float deathsTotal;
    private float deathsPVP;

    /// <summary>
    /// When this function is called, you need to set the string captainName to your name.
    /// </summary>
    protected abstract void SetCaptainName();

    public virtual void PreInitCaptain() {
        personalLog.Init();
    }

    /// <summary>
    /// This function is called in Ship.Start() and can be used to clear out any data from a previous ship.
    /// For example: Clearing out old navPath data.
    /// </summary>
    public virtual void InitCaptain() { }
    
    /// <summary>
    /// This function is called in Ship.OnDestroy() and can be used to clear out any data from a previous ship.
    /// For example: Clearing out old navPath data.
    /// </summary>
    public virtual void CleanUpCaptain() { }

    public virtual void PostCleanUpCaptain() {
        personalLog?.CleanUp();
    }
    
    /// <summary>
    /// <para>This function is called every FixedUpdate, and it is the only way that your Captain
    ///  can give commands to the ship. See CS_JGB_Merchant_2_SO and CS_HumanPlayer_SO for examples.</para>
    /// <para>Ship.eCommands can be found in the Ship script and include:</para>
    /// <para><b>none</b> - Do nothing</para>
    /// <para><b>sailsUp</b> - Raise the sails to Accelerate</para>
    /// <para><b>sailsDown</b> - Drop the sails to Decelerate</para>
    /// <para><b>turnLeft</b> - Turn left, decreasing the heading</para>
    /// <para><b>turnRight</b> - Turn right, increasing the heading</para>
    /// <para><b>turnToHeading, float toHeading</b> – Turn toward toHeading</para>
    /// <para><b>fire</b> - Fire all cannon pairs randomly (only the starboard or port cannon of each pair will fire)</para>
    /// <para><b>fireAtPoint, Vector3 aimPoint</b> - Attempt to aim cannon at a specific point in world space. Cannon aim angle is limited by SETTINGS</para>
    /// </summary>
    public abstract List<Ship.Command> AIUpdate(SenseInfo selfInfo, List<SenseInfo> sensed);

    protected virtual void OnValidate() {
        SetCaptainName();
    }
    protected virtual void Reset() {
        SetCaptainName();
    }
    
    // protected override void OnValidate() {
    //     base.OnValidate();
    //     SetCaptainName();
    // }
    // protected override void Reset() {
    //     base.Reset();
    //     SetCaptainName();
    // }

    public virtual void OnDrawGizmos() {
        // Do nothing
    }

    public virtual void OnDrawGizmosSelected() {
        // Do nothing
    }

    protected bool isPlaying => Application.isPlaying;
    [BoxGroup("PersonalLog")][SerializeField]
    protected XnLog _personalLog;
    public XnLog personalLog {
        get { return _personalLog; }
    }

}
