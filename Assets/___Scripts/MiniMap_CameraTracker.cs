using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XnTools;

[RequireComponent(typeof(UnityEngine.UI.Image))]
public class MiniMap_CameraTracker : MonoBehaviour {

    // public const float seaMapResolution = 1;
    // public const float miniMapResolution = 1;

    public int  minSizeInPixels = 32;
    public Rect mapRect;
    
    private Camera    cam;
    private Transform camTrans;

    RectTransform  rTrans;
    Image img;

    void Start() {
        cam = Camera.main;
        camTrans = cam.transform;
        rTrans = GetComponent<RectTransform>();
    }
    
    void Update() {
        // Find the point at which the Camera is looking
        Vector3 camFwd = camTrans.forward;
        // However, force the camera to look down at least as much as forward
        if ( Mathf.Abs( camFwd.z ) > Mathf.Abs( camFwd.y ) ) {
            if ( camFwd.y >= 0 ) camFwd.y = -1; // Accounts for times when the camera was looking directly along +Z or toward +Y
            camFwd.z = Mathf.Abs( camFwd.y ); // The camera up will always be +Z
            camFwd.Normalize(); // Make sure it's still 
        }
        // Find the point at which camFwd hits y=0
        Ray camRay = new Ray( camTrans.position, camFwd );
        Plane groundPlane = new Plane( Vector3.up, 0 );
        float distToGround;

        if ( !groundPlane.Raycast( camRay, out distToGround ) ) {
            // Do this if somehow the camera doesn't seem to point at the ground
            Vector2 shipPos = new Vector2(camTrans.position.x, camTrans.position.z);
            shipPos *= MiniMap.RESCALE; //shipPos /= seaMapScale * seaMapResolution * 0.5f;
            rTrans.anchoredPosition = shipPos;// rTrans.anchoredPosition = shipPos * miniMapResolution;
            return;
        }
        
        // Ok, the camRay did hit the ground. Now determine the height and width of the camera
        // Vector3 camCenter = cam.ViewportToWorldPoint( new Vector3( .5f, .5f, distToGround ) );
        // Vector3 camTopRight = cam.ViewportToWorldPoint( new Vector3( 1, 1, distToGround ) );
        Vector3 camLeftBottom = cam.ViewportToWorldPoint( new Vector3( 0, 0, distToGround ) );
        Vector3 camLeftTop = cam.ViewportToWorldPoint( new Vector3( 0, 1, distToGround ) );
        Vector3 camRightBottom = cam.ViewportToWorldPoint( new Vector3( 1, 0, distToGround ) );
        float worldW = (camRightBottom - camLeftBottom).magnitude;
        float worldH = (camLeftTop - camLeftBottom).magnitude;
        // Convert these world distances to minimap distances
        float mapW = worldW * MiniMap.RESCALE;
        float mapH = worldH * MiniMap.RESCALE;
        if ( mapW < minSizeInPixels ) mapW = minSizeInPixels;
        if ( mapH < minSizeInPixels ) mapH = minSizeInPixels;

        Vector3 camCenter = camRay.GetPoint( distToGround );
        Vector2 mapPos = new Vector2( camCenter.x, camCenter.z );
        mapPos *= MiniMap.RESCALE;
        
        rTrans.anchoredPosition = mapPos;
        rTrans.sizeDelta = new Vector2( mapW, mapH );
        mapRect = rTrans.rect; // rTrans.rect gives coordinates relative to the center of the rTrans itself
        mapRect.center = mapPos;
        // mapRect = new Rect( mapPos, rTrans.sizeDelta );
        float halfSize = MiniMap.MINIMAP_SIZE * 0.5f + 4; // 4 is the width of the frame itself
        Rect newMapRect = mapRect.Limit(-halfSize, halfSize, -halfSize, halfSize);
        if ( newMapRect != mapRect ) {
            rTrans.anchoredPosition = newMapRect.center;
            rTrans.sizeDelta = newMapRect.size;
        }
    }

    public void SetColor( Color col ) {
        if ( img == null ) img = GetComponent<Image>();
        img.color = col;
    }

    // public void SetSprite(Sprite sprite) {
    //     if ( img == null ) img = GetComponent<Image>();
    //     img.sprite = sprite;
    // }
    //
    // public void SelfDestruct() {
    //     Destroy( gameObject );
    // }
    
    
}
