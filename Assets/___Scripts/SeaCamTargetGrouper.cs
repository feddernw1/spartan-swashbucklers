using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using XnTools;
using Target = Cinemachine.CinemachineTargetGroup.Target; 

public class SeaCamTargetGrouper : MonoBehaviour {
    static private SeaCamTargetGrouper _S;

    public CinemachineVirtualCameraBase singleCam, groupCam;
    
    public  Transform mapTarget;
    public  float     mapTargetMoveDuration = .5f;
    private Vector3   mapTargetToPos, mapTargetFromPos;
    private float     mapTargetMoveStartTime;
    private bool      mapTargetMoving = false;
    
    public enum eCamFocusMode { captains, manual, map };
    private eCamFocusMode focusMode = eCamFocusMode.captains; 

    public CinemachineTargetGroup cineTargetGroup;
    public float                  updateFocusInterval = 0.5f;
    
    [SerializeField] List<CaptainRec> captsFocused;
    private          int              lastCaptsFocusedCount = 0;

    private Target[] mapTargetArray, captTargetArray;

    private TransformRotLock rotLock;
    

    private void Awake() {
        _S = this;

        Target targ = new Target() { radius = 1, target = mapTarget, weight = 1 };
        mapTargetArray = new Target[] { targ };

        rotLock = Camera.main.GetComponent<TransformRotLock>();
        StartCoroutine( UpdateFocusCoRo() );
    }

    void MoveMapTarget( Vector3 newPos ) {
        mapTargetMoving = true;
        mapTargetFromPos = mapTarget.transform.position;
        mapTargetToPos = newPos;
        mapTargetToPos.y = 0;
        mapTargetMoveStartTime = Time.time;
        // if ( rotLock != null ) {
        //     rotLock.lockValues.x = Mathf.RoundToInt( rotLock.transform.eulerAngles.x );
        //     rotLock.lockX = true;
        // }
    }

    void Update() {
        if ( mapTargetMoving ) {
            float u = (Time.time - mapTargetMoveStartTime) / mapTargetMoveDuration;
            if ( u >= 1 ) {
                u = 1;
                mapTargetMoving = false;
                // if ( rotLock != null ) {
                //     rotLock.lockX = false;
                // }
            }
            Vector3 pos = (1 - u) * mapTargetFromPos + u * mapTargetToPos;
            mapTarget.transform.position = pos;
        }
    }


    IEnumerator UpdateFocusCoRo() {
        while ( true ) {
            yield return new WaitForSeconds( updateFocusInterval );
            UpdateFocus();
        }
    }

    void UpdateCaptainsList(List<CaptainRec> tCapts = null) {
        if ( tCapts != null ) {
            captsFocused = tCapts;
        }
        UpdateFocus();
    }

    void UpdateFocus() {
        // cineTargetGroup.
        // Move the mapTarget if we're switching to 
        //     _S.captsFocused = tCapts;
        
        switch ( focusMode ) {
        case eCamFocusMode.captains:
            List<Target> targetList = new List<Target>();
            Target targ;
            foreach ( CaptainRec cap in captsFocused ) {
                if ( cap.currentShip == null ) continue;
                targ = new Target();
                targ.target = cap.currentShip.transform;
                targ.weight = 1;
                targ.radius = 50;
                targetList.Add( targ );
            }
            captTargetArray = targetList.ToArray();
            UpdateActiveVirtualCamera();
            break;
        }
    }

    void UpdateActiveVirtualCamera() {
        if ( _S == null ) return;
        switch ( captsFocused.Count ) {
        case 0: // The map should set focus
            if ( lastCaptsFocusedCount > 0 ) {
                mapTarget.transform.position = cineTargetGroup.transform.position;
            }
            groupCam.enabled = false;
            singleCam.enabled = true;
            cineTargetGroup.m_Targets = mapTargetArray;
            break;
        case 1: // The single captain should set focus
            groupCam.enabled = false;
            singleCam.enabled = true;
            cineTargetGroup.m_Targets = captTargetArray;
            break;
        default: // Any number of captains ≥ 2 should use the groupCam
            // Multiple captains are focused, so we should use the group camera
            groupCam.enabled = true;
            singleCam.enabled = false;
            cineTargetGroup.m_Targets = captTargetArray;
            break;
        }
        lastCaptsFocusedCount = captsFocused.Count;
    }
    
    
    
    public static void UpdateCaptainsFocused( List<CaptainRec> tCapts = null ) {
        if ( _S == null ) return;
        _S.UpdateCaptainsList( tCapts );
        // if ( tCapts != null ) {
        //     _S.UpdateFocus(tCapts);
            // _S.captsFocused = tCapts;
        // }
    }

    public static Transform GetMapTarget() {
        if ( _S == null ) return null;
        return _S.mapTarget;
    }

    public static void MoveMapFocus( Vector3 newPos ) {
        if ( _S == null ) {
            Debug.LogError("There is no singleton of SeaCamTargetGrouper, so clicking the map will not move the camera.");
            return;
        }
        _S.MoveMapTarget( newPos );
    }
}
