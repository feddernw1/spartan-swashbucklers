using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(UnityEngine.UI.Image))]
public class MiniMap_Tracker : MonoBehaviour {

    // public const float seaMapResolution = 1;
    // public const float miniMapResolution = 1;

    public Transform trackedTransform;

    RectTransform  rTrans;
    Image img; 
    
    void Update() {
        Vector2 shipPos = new Vector2(trackedTransform.position.x, trackedTransform.position.z);
        shipPos *= MiniMap.RESCALE; //shipPos /= seaMapScale * seaMapResolution * 0.5f;
        if (rTrans == null) rTrans = GetComponent<RectTransform>();
        rTrans.anchoredPosition = shipPos;// rTrans.anchoredPosition = shipPos * miniMapResolution;
        rTrans.localEulerAngles = new Vector3(0, 0, -trackedTransform.eulerAngles.y);
    }

    public void SetColor( Color col ) {
        if ( img == null ) img = GetComponent<Image>();
        img.color = col;
    }

    public void SetSprite(Sprite sprite) {
        if ( img == null ) img = GetComponent<Image>();
        img.sprite = sprite;
    }

    public void SelfDestruct() {
        Destroy( gameObject );
    }
}
