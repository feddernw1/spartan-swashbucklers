using System;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEditor;

[SelectionBase]
public class SeaTerrain_GPUInstanced : MonoBehaviour {
    public GameObject                      cubePrefab; // Prefab of the cube object
    public Material                        cubeMaterial; // Material to use for the cubes
    public SpartanSwashbucklersSettings_SO settings;

    private Transform trans;


    [SerializeField]
    [HideInInspector]
    private Matrix4x4[] matrices; // Array to store the transformation matrices
    [SerializeField]
    [HideInInspector]
    private GameObject[] gameObjects;
    [SerializeField]
    [HideInInspector]
    private Renderer[] rends; // Renderers block for setting per-instance properties


    void Start() {
        AssignPropertyBlocks();
    }

    [Button()]
    public void GenerateCubes() {
        DestroyAllChildGameObjects();
        
        // Make sure everything is set up properly.
        if ( settings == null || settings.seaMapTexture2D == null ) {
            Debug.LogError(
                "The SpartanSwashbucklersSettings_SO scriptable object must be assigned, and it must have " +
                "a valid saeMapTexture2D before this code can be called." );
            return;
        }

        trans = transform;
        Texture2D tex = settings.seaMapTexture2D;
        Color32[] colors = tex.GetPixels32( 0 );
        gameObjects = new GameObject[tex.height * tex.width];
        rends = new Renderer[gameObjects.Length];
        
        Vector3 localScale = trans.localScale;
        // trans.localScale = Vector3.one; // Set localScale to 1 so that instantiated cubes are positioned properly

        Vector3 pos = Vector3.zero;
        Quaternion rot = Quaternion.identity;
        float halfH = tex.height / 2 - 0.5f;
        float halfW = tex.width  / 2 - 0.5f;
        int ndx;
        // Instantiate the cubes with GPU instancing
        for ( int y = 0; y < tex.height; y++ ) {
            for ( int x = 0; x < tex.width; x++ ) {
                ndx = y * tex.width + x;

                pos.x = x - halfW;
                pos.y = ( colors[ndx].a - 256 ) / 256f;
                pos.z = y - halfH;

                // Instantiate the cube with the transformation matrix and property block
                gameObjects[ndx] = Instantiate( cubePrefab, pos, rot );
                gameObjects[ndx].transform.SetParent( trans, false );
                rends[ndx] = gameObjects[ndx].GetComponent<Renderer>();
                rends[ndx].material = cubeMaterial;
            }
        }

        trans.localScale = localScale; // Return to actual localScale
        
        AssignPropertyBlocks();

    }

    [Button()]
    void DestroyAllChildGameObjects() {
        if ( gameObjects != null ) {
            for ( int i = 0; i < gameObjects.Length; i++ ) {
                DestroyImmediate(gameObjects[i]);
            }
        }
        gameObjects = null;
        foreach ( Transform t in transform ) {
            DestroyImmediate( t.gameObject );
        }
        Debug.Log( $"All children of {gameObject.name} should be destroyed now." );
    }

#if UNITY_EDITOR
    private void OnValidate() {
        EditorApplication.delayCall += this.TryAssignPropertyBlocks;
    }
#endif
    private bool propertyBlocksAssigned = false;
    private void OnDrawGizmos() {
        if ( !propertyBlocksAssigned ) {
            propertyBlocksAssigned = true;
            TryAssignPropertyBlocks();
        }
    }

    void TryAssignPropertyBlocks() {
        // Make sure everything is set up properly.
        if ( settings == null || settings.seaMapTexture2D == null ) {
            Debug.LogError(
                "The SpartanSwashbucklersSettings_SO scriptable object must be assigned, and it must have " +
                "a valid saeMapTexture2D before this code can be called." );
            return;
        }
        if ( gameObjects           == null || rends == null || gameObjects.Length == 0
             || gameObjects.Length != rends.Length ) {
            GenerateCubes();
            return;
        }
        AssignPropertyBlocks();
    }

    void AssignPropertyBlocks() {
        Texture2D tex = settings.seaMapTexture2D;
        Color32[] colors = tex.GetPixels32( 0 );
        Color32 col;
        MaterialPropertyBlock propBlock = new MaterialPropertyBlock();
        int ndx;
        // Assign MaterialPropertyBlocks to the cubes with GPU instancing
        for ( int y = 0; y < tex.height; y++ ) {
            for ( int x = 0; x < tex.width; x++ ) {
                ndx = y * tex.width + x;

                col = colors[ndx];
                col.a = 255;

                // Set the color in the propertyBlock
                propBlock.SetColor( "_MainColor", col );
                rends[ndx].material = cubeMaterial;
                rends[ndx].SetPropertyBlock( propBlock );
            }
        }
        
        // Enable GPU instancing on the material
        cubeMaterial.enableInstancing = true;
    }
}


// From ChatGPT - 2023-04-18
/*
using UnityEngine;

public class CubeManager : MonoBehaviour
{
    public GameObject cubePrefab; // Prefab of the cube object
    public int        numberOfCubes = 4000; // Number of cubes to render
    public Material   cubeMaterial; // Material to use for the cubes

    private Matrix4x4[]           matrices; // Array to store the transformation matrices
    private MaterialPropertyBlock propertyBlock; // Property block for setting per-instance properties

    void Start()
    {
        // Initialize the matrices array and property block
        matrices = new Matrix4x4[numberOfCubes];
        propertyBlock = new MaterialPropertyBlock();

        // Instantiate the cubes with GPU instancing
        for (int i = 0; i < numberOfCubes; i++)
        {
            // Generate a random position and rotation for each cube
            Vector3 position = new Vector3(Random.Range(-10f, 10f), Random.Range(0f, 10f), Random.Range(-10f, 10f));
            Quaternion rotation = Quaternion.Euler(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f));

            // Generate a random color for each cube
            Color color = new Color(Random.value, Random.value, Random.value);

            // Set the transformation matrix and color in the matrices array
            matrices[i] = Matrix4x4.TRS(position, rotation, Vector3.one);
            propertyBlock.SetColor("_Color", color);

            // Instantiate the cube with the transformation matrix and property block
            GameObject cube = Instantiate(cubePrefab, position, rotation);
            Renderer renderer = cube.GetComponent<Renderer>();
            renderer.material = cubeMaterial;
            renderer.SetPropertyBlock(propertyBlock);
        }

        // Enable GPU instancing on the material
        cubeMaterial.enableInstancing = true;
    }
}
*/