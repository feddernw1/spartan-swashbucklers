using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using XnTools;

public class MiniMap : MonoBehaviour {
    static private MiniMap   _S;
    static public  Transform TRANS { get; private set; }
    public const   float     MINIMAP_SIZE = 364;
    public const   float     MAP_SIZE     = 64;
    public const   float     MAP_SCALE    = 200;
    public const   float     RESCALE      = MINIMAP_SIZE / MAP_SIZE / MAP_SCALE;
    public const   float     DESCALE      = MAP_SCALE * MAP_SIZE / MINIMAP_SIZE;
    
    static public  Dictionary<Transform, GameObject> TRACKER_DICT = new Dictionary<Transform, GameObject>();

    public GameObject               trackerPrefab;
    [SerializeField] public MapTrackerTypeToSprite[] mapTrackerTypeSprites;
    
    private void Awake() {
        if ( _S != null ) {
            Debug.LogError( "Like the Highlander, there can be only one SeaMap!" );
            Destroy( gameObject );
            return;
        }
        _S = this;
        TRANS = transform;
    }

    public static void TRACK_ON_MAP( eMapTrackerType tType, Transform trans ) {
        if ( TRACKER_DICT.ContainsKey( trans ) ) {
            Debug.LogWarning( $"{trans.gameObject.name} is already in the TRACKER_DICT! I'm not adding it again." );
            return;
        }
        if ( _S == null ) {
            Debug.LogError( $"TRACK_ON_MAP() was called by {trans.gameObject.name} but MiniMap._S was still null!" );
            return;
        }
        GameObject trackerGO = Instantiate<GameObject>( _S.trackerPrefab, TRANS );
        if ( tType == eMapTrackerType.swashbuckler ) {
            // This ensures that Swashbucklers render on top in the MiniMap view
            trackerGO.transform.SetAsLastSibling();
        } else {
            trackerGO.transform.SetAsFirstSibling();
        }
        MiniMap_Tracker tracker = trackerGO.GetComponent<MiniMap_Tracker>();
        tracker.trackedTransform = trans;
        foreach ( MapTrackerTypeToSprite mTS in _S.mapTrackerTypeSprites ) {
            if ( mTS.type == tType ) {
                tracker.SetSprite( mTS.sprite );
                tracker.SetColor( mTS.color );
                break;
            }
        }
        // Try to get the ship ID
        Ship s = trans.GetComponent<Ship>();
        if ( s != null ) tracker.gameObject.name = $"S{s.shipID} - Tracker";
        
        TRACKER_DICT.Add( trans, trackerGO );
    }

    public static void REMOVE_FROM_MAP( Transform trans ) {
        if ( TRACKER_DICT.ContainsKey( trans ) ) {
            GameObject trackerGO;
            TRACKER_DICT.Remove( trans, out trackerGO );
            Destroy( trackerGO );
            return;
        } else {
            Debug.LogWarning( $"{trans.gameObject.name} attempted to remove itself from TRACKER_DICT, but it wasn't in there." );
        }
    }
    
    

    public enum eMapTrackerType { none, swashbuckler, merchant };

    [System.Serializable]
    public class MapTrackerTypeToSprite {
        [Hidden] public string typeString;
        [OnValueChanged( "UpdateTypeString" )][AllowNesting]
        public eMapTrackerType type;
        public Color color;
        [ShowAssetPreview( 16, 16 )] [AllowNesting]
        public Sprite sprite;
        
        void UpdateShipClassString() {
            typeString = XnUtils.NicifyVariableName( type.ToString() );
        }
    }
}
