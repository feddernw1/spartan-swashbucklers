using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

// ScreenCapture Documentation:
// https://docs.unity3d.com/ScriptReference/ScreenCapture.CaptureScreenshot.html

public class ScreenshotButton : MonoBehaviour {
    static private ScreenshotButton _S;
    
    [Range( 1, 8 )]
    public int superSize = 2;
    public string fileNamePrefix = DEFAULT_PREFIX;

    private void Awake() {
        _S = this;
    }

#if UNITY_WEBGL && !UNITY_EDITOR
    void Start() {
        // WebGL can't take screenshots or save files, so turn off the capture button. - JGB 2024-04-19
        gameObject.SetActive( false );
    }
#endif
    
    [Button]
    public void ClickToTakeScreenshot() {
        Capture( superSize, fileNamePrefix );
    }

    private const string DEFAULT_PREFIX = "Capture";
    
    static public void Capture( int superSize = 1, string fileNamePrefix = DEFAULT_PREFIX, string filePath = "" ) {
        // Check for a Singleton to override the defaults. - JGB 2024-04-19
        if ( _S != null ) {
            if ( fileNamePrefix == DEFAULT_PREFIX ) {
                fileNamePrefix = _S.fileNamePrefix;
            }
            if ( superSize == 1 ) {
                superSize = _S.superSize;
            }
        }
        
        string fileName = $"{fileNamePrefix}-{System.DateTime.Now.ToDateTimeStamp()}.png";
        ScreenCapture.CaptureScreenshot( fileName, superSize );

        Vector2 gameViewSize;
#if UNITY_EDITOR
        gameViewSize = UnityEditor.Handles.GetMainGameViewSize();
        Debug.LogWarning( $"Saved \"{fileName}\" "
                          + $"at {gameViewSize.x * superSize}x{gameViewSize.y * superSize}"
                          + " to project folder (the parent folder of Assets) !" );
#else
        gameViewSize = new Vector2( Screen.width, Screen.height );
        Debug.LogWarning( $"Saved \"{fileName}\" "
                   + $"at {gameViewSize.x * superSize}x{gameViewSize.y * superSize}"
                   + $" to path: \"{filePath}\" !" );
#endif
    }
}

public static class DateTimeExtension {
    static public string ToTimeStamp( this System.DateTime time ) {
        string str = $"{time.Hour:00}{time.Minute:00}";
        return str;
    }
    static public string ToDateStamp( this System.DateTime time ) {
        string str = $"{time.Year:0000}-{time.Month:00}-{time.Day:00}";
        return str;
    }
    static public string ToDateTimeStamp( this System.DateTime time) {
        return $"{time.ToDateStamp()}_{time.ToTimeStamp()}";
    }
}