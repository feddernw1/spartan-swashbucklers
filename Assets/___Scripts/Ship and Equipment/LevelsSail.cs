using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelsSail : MonoBehaviour {
    [Range( 0, 3 )]
    public int level = 0;
    public List<SailGroup> groups;

    // Update is called once per frame
    void ShowLevel() {
        for ( int i = 0; i < groups.Count; i++ ) {
            if ( i <= level ) {
                foreach ( Sail c in groups[i].items ) {
                    c.isOn = true;
                    c.atLevel = ( i == level );
                }
            } else {
                foreach ( Sail c in groups[i].items ) {
                    c.isOn = false;
                }
            }
        }
    }

#if UNITY_EDITOR
    private void OnValidate() {
        ShowLevel();
    }
#endif

    [System.Serializable]
    public class SailGroup {
        public List<Sail> items;
    }
}
