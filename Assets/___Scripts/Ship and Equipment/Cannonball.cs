using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannonball : MonoBehaviour {
    static private float   cannonShotSpeed = 0;
    static private Vector2 cannonRangeAndHeight;
    static public  float   DAMAGE { get; private set; }
    // static private AnimationCurve cannonFlightHeightCurve;
    
    //TODO: The Cannonball needs to know who fired it!

    [Tooltip("This is designed for 0.5 to be the height at which it was shot and 0 to be cannonRangeAndHeight.y/2 below that.")]
    public AnimationCurve heightCurve;

    [Range(0,1)]
    public float shipVelocityApplied = 1;
    
    [HideInInspector]
    public Vector3 shipVelWhenFired = Vector3.zero;
    
    Vector3 posStart, dirStart, posFlat, yDelta;
    float distTraveled;

    [System.NonSerialized] public CaptainStrategy_SO captainSO;

    // Start is called before the first frame update
    void Start() {
        if ( cannonShotSpeed == 0 ) {
            cannonShotSpeed = GameManager.SETTINGS.cannonShotSpeed;
            cannonRangeAndHeight = GameManager.SETTINGS.cannonRangeAndHeight;
            DAMAGE = GameManager.SETTINGS.cannonballDamage;
            // cannonFlightHeightCurve = GameManager.SETTINGS.cannonFlightHeightCurve;
        }
        posStart = transform.position;
        dirStart = transform.forward;
        dirStart.y = 0;
        posFlat = posStart;
        shipVelWhenFired.y = 0;
        shipVelWhenFired *= shipVelocityApplied;
    }

    void FixedUpdate() {
        posFlat += ((dirStart * cannonShotSpeed) + shipVelWhenFired) * Time.fixedDeltaTime;
        // posFlat += dirStart * ( cannonShotSpeed * Time.fixedDeltaTime );

        // distTraveled is NOT affected by shipVelWhenFired so that Cannonballs are all in the air for the same amount of time
        distTraveled += (cannonShotSpeed * Time.fixedDeltaTime);
        float rangePercent = distTraveled / cannonRangeAndHeight.x;

        yDelta.y = cannonRangeAndHeight.y * (heightCurve.Evaluate( rangePercent ) - 0.5f);
        transform.position = posFlat + yDelta;

        if ( rangePercent >= 1 ) {
            DestroySelfAndDetachTrail();
            return;
        }
        
        // This section destroys Cannonballs that pass into land spaces
        Vector2Int thisMapLoc = SeaMap.MapLoc( new Vector2( this.transform.position.x, this.transform.position.z ) );

        if ( !SeaMap.IsMapLocValid( thisMapLoc ) ) { // If the Cannonball has gone outside the map boundaries…
            DestroySelfAndDetachTrail();
        } else if ( !SeaMap.NAVIGABLE[thisMapLoc.x, thisMapLoc.y] ) { // If the Cannonball is over land…
            // Debug.Log("not in navigable terrain, destroy this");
            DestroySelfAndDetachTrail();
        }
    }

    void DestroySelfAndDetachTrail() {
        // Detach the TrailRenderer
        if ( transform.childCount > 0 ) {
            Transform trailTrans = transform.GetChild( 0 );
            trailTrans.SetParent( Cannon._CANNONBALL_ANCHOR, true );
        }
        // transform.DetachChildren();
        Destroy( gameObject );
    }
}