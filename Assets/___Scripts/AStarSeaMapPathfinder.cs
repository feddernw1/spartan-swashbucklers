#define USE_SPARSE_GRID
// #define DEBUG_YIELDS

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XnTools;
using System.Diagnostics;
using Debug = UnityEngine.Debug;


public class AStarSeaMapPathfinder {
    private const float MAX_EXECUTION_MILLISECONDS_BEFORE_YIELD = 2;


    private const int diagonalCost   = 14;
    private const int orthogonalCost = 10;

    public Vector2Int startMapLoc, endMapLoc;

    // public bool                      allowDiagonals = false;
    public List<AStarComparable>          path;
    XnPriorityQueue<AStarComparable>      queue;
#if USE_SPARSE_GRID
    private XnSparseGrid<AStarComparable> aStarSparseGrid;
#else
    private AStarComparable[,]       aStarGrid;
#endif 
    
    static private Vector2Int[] neighborsOrthogonal = new[] {
        new Vector2Int( 1, 0 ), new Vector2Int( 0, 1 ), new Vector2Int( -1, 0 ), new Vector2Int( 0, -1 )
    };
    static private Vector2Int[] neighborsDiagonal = new[] {
        new Vector2Int( 1, 1 ), new Vector2Int( -1, 1 ), new Vector2Int( -1, -1 ), new Vector2Int( 1, -1 )
    };


    public IEnumerator PathFindCoRo( Vector2Int fromMapLoc, Vector2Int toMapLoc,
                              bool allowDiagonalMovement = true,
                              System.Action<List<AStarComparable>> callbackAction = null ) {

        // Choose a currNode and endNode
        startMapLoc = fromMapLoc;
        endMapLoc = toMapLoc;
        Init();

        AStarComparable currNode = new AStarComparable( startMapLoc );
        AStarComparable endNode = null;
        currNode.g = 0;
        currNode.h = GetHCost( currNode.mapLoc, endMapLoc );
#if USE_SPARSE_GRID
        aStarSparseGrid[currNode.mapLoc.x, currNode.mapLoc.y] = currNode;
#else
        aStarGrid[currNode.mapLoc.x, currNode.mapLoc.y] = currNode;
#endif
        queue.Enqueue( currNode, currNode.f, currNode.h );
        Stopwatch stopper = Stopwatch.StartNew();
        int yieldCount = 0;
        while ( queue.Count > 0 ) {
            currNode = queue.Dequeue();
            currNode.inQueue = false;
            if ( currNode.mapLoc == endMapLoc ) {
                // We found the last node!!!
                endNode = currNode;
                break;
            }
            // Find and add neighbors
            for ( int i = 0; i < neighborsOrthogonal.Length; i++ ) {
                CheckNeighbor( currNode, neighborsOrthogonal[i], orthogonalCost );
            }
            if ( allowDiagonalMovement ) {
                for ( int i = 0; i < neighborsOrthogonal.Length; i++ ) {
                    CheckNeighbor( currNode, neighborsDiagonal[i], diagonalCost );
                }
            }
            if ( stopper.ElapsedMilliseconds >= MAX_EXECUTION_MILLISECONDS_BEFORE_YIELD ) {
                yieldCount++;
                yield return new WaitForSeconds( 0.1f );
                stopper.Restart();
            }
        }
        path = new List<AStarComparable>( GeneratePath( endNode ) );
#if DEBUG_YIELDS
        Debug.LogWarning($"Pathfinding result length: {path.Count} #yields: {yieldCount}"  );
#endif        
        if ( callbackAction != null ) callbackAction( path );
    }

    void Init() {
        path = new List<AStarComparable>();
        queue = new XnPriorityQueue<AStarComparable>();
#if USE_SPARSE_GRID
        aStarSparseGrid = new XnSparseGrid<AStarComparable>();
#else
        aStarGrid = new AStarComparable[SeaMap.TEXTURE_RESOLUTION, SeaMap.TEXTURE_RESOLUTION];
#endif
    }
    

    void CheckNeighbor( AStarComparable currNode, Vector2Int offset, int costToThisNeighbor,
                        bool allowDiagonalMovement = true ) {
        Vector2Int neighborLoc = currNode.mapLoc + offset;

        // Abort if the neighbor would be outside the grid.
        if ( neighborLoc.x < 0 || neighborLoc.x >= SeaMap.TEXTURE_RESOLUTION ) return;
        if ( neighborLoc.y < 0 || neighborLoc.y >= SeaMap.TEXTURE_RESOLUTION ) return;
        if ( !SeaMap.NAVIGABLE[neighborLoc.x, neighborLoc.y] ) return;

        AStarComparable neighborNode;
#if USE_SPARSE_GRID
        if ( aStarSparseGrid[neighborLoc.x, neighborLoc.y] == null ) {
#else 
        if ( aStarGrid[neighborLoc.x, neighborLoc.y] == null ) {
#endif
            // Add a new node here
            neighborNode = new AStarComparable( neighborLoc );
            neighborNode.g = currNode.g + costToThisNeighbor;
            neighborNode.h = GetHCost( neighborLoc, endMapLoc, allowDiagonalMovement );
#if USE_SPARSE_GRID
            aStarSparseGrid[neighborLoc.x, neighborLoc.y] = neighborNode;
#else             
            // aStarGrid[neighborLoc.x, neighborLoc.y] = neighborNode;
#endif
            neighborNode.prevNode = currNode;
            queue.Enqueue( neighborNode, neighborNode.f, neighborNode.h );
            neighborNode.inQueue = true;
        } else {
#if USE_SPARSE_GRID
            neighborNode = aStarSparseGrid[neighborLoc.x, neighborLoc.y];
#else            
            neighborNode = aStarGrid[neighborLoc.x, neighborLoc.y];
#endif
            if ( neighborNode.inQueue ) {
                // Check the g cost
                if ( neighborNode.g > currNode.g + costToThisNeighbor ) {
                    // Need to update the cost of neighborNode
                    int ndx = queue.IndexOfValue( neighborNode );
                    queue.RemoveAt( ndx );
                    neighborNode.g = currNode.g + costToThisNeighbor;
                    neighborNode.prevNode = currNode;
                    queue.Enqueue( neighborNode, neighborNode.f, neighborNode.h );
                    neighborNode.inQueue = true;
                }
            }
        }
    }

    List<AStarComparable> GeneratePath( AStarComparable currNode ) {
        if ( currNode == null ) return new List<AStarComparable>();
        List<AStarComparable> aList = new List<AStarComparable>();
        do {
            if ( currNode.prevNode == null ) break;
            aList.Insert( 0, currNode );
            currNode = currNode.prevNode;
        } while ( currNode != null );
        return aList;
    }


    static public int GetHCost( AStarComparable a, AStarComparable b, bool allowDiagonalMovement=true ) {
        return GetHCost( a.mapLoc, b.mapLoc );
    }

    static public int GetHCost( Vector2Int a, Vector2Int b, bool allowDiagonalMovement=true ) {
        int hCost = 0;
        int x = Mathf.Abs( a.x - b.x );
        int y = Mathf.Abs( a.y - b.y );
        bool xGreater = ( x > y );
        if ( allowDiagonalMovement ) {
            if ( xGreater ) { hCost = diagonalCost * y + orthogonalCost * ( x - y ); } else {
                hCost = diagonalCost * x + orthogonalCost * ( y - x );
            }
        } else { hCost = ( x + y ) * orthogonalCost; }
        return hCost;
    }

    
}


[System.Serializable]
public class AStarComparable : System.IComparable {
    
    [SerializeField]
    private Vector2Int      _mapLoc;
    public Vector2Int mapLoc {
        get { return _mapLoc;}
        private set {
            _mapLoc = value;
            position = SeaMap.Position( value );
        }
    }
    
    public int g; // The cost to Get here
    public int h; // The Heuristic guess at a cost to the goal
    public int f => g + h; // The Full cost of this node
    
    public  Vector3         position { get; private set; }
    public  AStarComparable prevNode = null;

    public AStarComparable( Vector2Int tMapLoc ) {
        mapLoc = tMapLoc;
    }

    public bool inQueue = false;

    public int CompareTo( object obj ) {
        if ( !( obj is AStarComparable ) ) return -1;
        AStarComparable that = obj as AStarComparable;
        if ( this.f < that.f ) return -1;
        if ( this.f > that.f ) return 1;
        if ( this.h < that.h ) return -1;
        if ( this.h > that.h ) return 1;
        return 0;
    }

    public override string ToString() {
        return $"[ {_mapLoc.x}, {_mapLoc.y} ]";
    }
}


/*
// Basic Theta* AreNodesInLineOfSight() code from Chat-GPT. 2023-04-22 
private bool AreNodesInLineOfSight(GridNode node1, GridNode node2, GridNode endNode, bool[,] navigableGrid)
{
    int x1 = node1.x;
    int y1 = node1.y;
    int x2 = node2.x;
    int y2 = node2.y;

    int dx = Math.Abs(x2 - x1);
    int dy = Math.Abs(y2 - y1);

    int sx = x1 < x2 ? 1 : -1;
    int sy = y1 < y2 ? 1 : -1;

    int err = dx - dy;
    int currentX = x1;
    int currentY = y1;

    while (currentX != x2 || currentY != y2)
    {
        int e2 = err << 1;

        if (e2 > -dy)
        {
            err -= dy;
            currentX += sx;
        }

        if (e2 < dx)
        {
            err += dx;
            currentY += sy;
        }

        if (!navigableGrid[currentX, currentY])
        {
            // If there is a blocked cell in the line of sight, return false
            return false;
        }
    }

    // If no blocked cells were encountered, return true
    return true;
}
*/