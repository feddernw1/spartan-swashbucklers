using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[DefaultExecutionOrder(1000)]
public class ScaleToCamera : MonoBehaviour {
    private Transform  camTrans;
    
    public  GameObject child;

    [Header("Scale to Camera")]
    public Vector2 minMaxScale;
    public Vector2 camHeightForScale;
    public bool    hideAtMin = false;
    public bool    lockX, lockY, lockZ;

    [Header( "Height to Camera" )]
    public Vector2 minMaxHeight;
    public Vector2 camHeightForHeight;
    

    void Start() {
        camTrans = Camera.main.transform;
    }


    private float uScalePrev = -1, uHeightPrev = -1;
    
    void FixedUpdate() {
        float camHeight = camTrans.position.y;
        float uScale = Mathf.InverseLerp( camHeightForScale[0], camHeightForScale[1], camHeight );
        if ( uScale != uScalePrev ) {
            if ( uScale == 0 ) {
                if (hideAtMin) child.SetActive( false );
            } else {
                child.SetActive( true );
                Vector3 scaleVec =  Vector3.one * Mathf.Lerp( minMaxScale[0], minMaxScale[1], uScale );
                if (lockX) scaleVec.x = 1;
                if (lockY) scaleVec.y = 1;
                if (lockZ) scaleVec.z = 1;
                child.transform.localScale = scaleVec;
            }
            uScalePrev = uScale;
        }
        
        
        float uHeight = Mathf.InverseLerp( camHeightForHeight[0], camHeightForHeight[1], camHeight );
        if ( uHeight != uHeightPrev ) {
            Vector3 pos = child.transform.localPosition;
            pos.y = Mathf.Lerp( minMaxHeight[0], minMaxHeight[1], uHeight );
            child.transform.localPosition = pos;
            uHeightPrev = uHeight;
        }
    }
    
}
