using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AssignSeaMapFromSO : MonoBehaviour {
    public SpartanSwashbucklersSettings_SO settingsSO;

    [SerializeField]
    private Renderer rend;
    [SerializeField]
    private RawImage image;

    private void OnDrawGizmos() {
        if ( rend == null && image == null ) {
            rend = GetComponent<Renderer>();
            image = GetComponent<RawImage>();
        }
        UpdateImage();
    }

    void Awake() {
        rend = GetComponent<Renderer>();
        image = GetComponent<RawImage>();
        UpdateImage();
    }

    void UpdateImage() {
        if ( rend != null ) {
            if ( rend.sharedMaterial.mainTexture != settingsSO.seaMapTexture2D ) {
                rend.sharedMaterial.mainTexture = settingsSO.seaMapTexture2D;
            }
        }
        if ( image != null ) {
            if ( image.texture != settingsSO.seaMapTexture2D ) {
                image.texture = settingsSO.seaMapTexture2D;
            }
        }
    }
}
