using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XnTools;
using NaughtyAttributes;

public class MasterGunner : MonoBehaviour {
    private const float randomizedFireDelay = 0.25f;

    public CannonPair[] cannonPairs;
    
    [SerializeField][NaughtyAttributes.ReadOnly]
    private Ship ship;

    public void SetShip( Ship tShip ) {
        ship = tShip;
    }

    void Start() {
        Rigidbody shipRB = ship.GetComponent<Rigidbody>();
        foreach ( CannonPair cp in cannonPairs ) {
            cp.port.SetShipRigidbody(shipRB);
            cp.starboard.SetShipRigidbody(shipRB);
        }
    }

    private Vector3 aimPoint = Vector3.negativeInfinity;
    
    // [BoxGroup("Set by Spartan Swashbuckler Settings")]
    // public float randomizedFireDelay = 0.25f;
    
    List<CannonPair> cannonToFire = new List<CannonPair>();
    
    // TODO: Add reloading of cannon to MasterGunner and rewrite Fire methods
    
    public void Fire(int numToFire) {
        if ( numToFire <= 0 ) {
            aimPoint = Vector3.negativeInfinity;
            return;
        }
        if ( numToFire > cannonPairs.Length ) {
            Debug.LogError( $"Ship asked {numToFire} cannon to fire, but cannonPairs only contains {cannonPairs.Length} CannonPairs. I'm firing all the cannon I have!" );
            numToFire = cannonPairs.Length;
        }
        
        CannonPair canPair;
        cannonToFire.Clear();
        cannonToFire.AddRange( cannonPairs[0..numToFire] ); // Cool new C# 8 syntax for copying part of an array!
        if (cannonToFire.Count == 0) return;

        for ( int i = 0; i<numToFire; i++ ) {
            canPair = cannonToFire.RemoveRandom();
            if ( i == 0 ) {
                canPair.Fire(aimPoint, 0);
            } else {
                canPair.Fire(aimPoint, Random.Range(0, randomizedFireDelay));
            }
        }
        aimPoint = Vector3.negativeInfinity;
    }

    public void FireAtPoint( int numToFire, Vector3 tAimPoint ) {
        aimPoint = tAimPoint;
        Fire( numToFire );
    }

    [System.Serializable]
    public class CannonPair {
        public Cannon port;
        public Cannon starboard;

        // Caches for the Shot Point Transforms on either side
        private Transform portSPTrans, starboardSPTrans;

        public Cannon this[int cannonNum] {
            get {
                if ( cannonNum == 0 ) return port;
                if ( cannonNum == 1 ) return starboard;
                return null;
            }
        }

        public void Fire( Vector3 aimPoint, float timeToFire ) {
            if ( aimPoint != Vector3.negativeInfinity ) {
                // Attempt to find the best side to shoot
                if ( starboard.dotForward( aimPoint ) > 0 ) {
                    starboard.Fire( aimPoint, timeToFire );
                    return;
                } else if (port.dotForward( aimPoint ) > 0 ) {
                    port.Fire( aimPoint, timeToFire );
                    return;
                }
                // If it is somehow in front of neither (like if it's directly in front of the ship
                //  then just randomly select a side to fire as if there was no aimPoint.
            }
            // If there is no aimPoint, the side which shoots is randomly selected
            if ( XnUtils.randomBool ) port.Fire( aimPoint, timeToFire );
            else starboard.Fire( aimPoint, timeToFire );
        }
    }

    private CaptainStrategy_SO captainSO;
    public void SetCaptain( CaptainStrategy_SO csso ) {
        captainSO = csso;
        foreach ( CannonPair cp in cannonPairs ) {
            cp.starboard.captainSO = csso;
            cp.port.captainSO = csso;
        }
    }
}
