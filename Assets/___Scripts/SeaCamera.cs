using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using XnTools;

public class SeaCamera : MonoBehaviour {
    private static SeaCamera _S;

    [Header( "Inscribed" )]
    public Transform poi;
    public float panSpeed = 20;
    public float rotateSpeed = 0.2f;
    public float easing = 0.15f;
    public Transform cameraPitch;
    public Transform cameraDolly;
    [MinMaxSlider( 0, 90 )]
    public Vector2 zoomPitches = new Vector2( 57, 90 );
    [MinMaxSlider( 100, 10000 )]
    public Vector2Int zoomDistances = new Vector2Int( 10, 100 );
    [Range( 0, 10 )]
    public float zoomEasingDuration = 5;
    public XnInterpolation.CurveItem zoomCurve = new XnInterpolation.CurveItem(eCurveType.powInv, 2);

    [Header("Dynamic")]
    public Vector3 targetPos = Vector3.zero;
    [Range(0, 1)] public float zoomLerp = 0;
    [NaughtyAttributes.ReadOnly][Range( 0, 1 )]
    public float zoomLerpCurved;
    [Range( -180, 180 )]
    public float rotationY = 0;
    [NaughtyAttributes.ReadOnly]
    public float pitch, dist;

    Camera cam;

    private void Awake() {
        _S = this;
    }

    void FixedUpdate() {
        if ( easingZoom ) {
            float u = ( Time.time - zoomEasingStart ) / zoomEasingDuration;
            if ( u >= 1 ) {
                u = 1;
                easingZoom = false;
            }
            zoomLerp = fromZoomLerp.LerpTo( toZoomLerp, u );
        }
        
        Vector3 toPos = targetPos;
        if ( poi != null ) {
            targetPos = poi.position;
            toPos = Vector3.Lerp( targetPos, toPos, (zoomLerp*1.5f)-0.5f  );
        }

        zoomLerpCurved = zoomCurve.Evaluate( zoomLerp );

        //Vector3 toPos = Vector3.zero;
        
        if ( poi != null ) { toPos = Vector3.Lerp( poi.position, toPos, ( zoomLerpCurved * 1.5f ) - 0.5f ); }
        transform.position = Vector3.Lerp( transform.position, toPos, easing );

        if ( cameraDolly != null && cam == null ) { cam = cameraDolly.GetComponent<Camera>(); }
        transform.rotation = Quaternion.Slerp( Quaternion.Euler( 0, rotationY, 0 ), Quaternion.identity, zoomLerpCurved );
        pitch = zoomPitches.x.LerpTo( zoomPitches.y, zoomLerpCurved );
        dist = ( (float) zoomDistances.x ).LerpTo( zoomDistances.y, zoomLerpCurved );
        cameraPitch.rotation = Quaternion.Euler( pitch, rotationY, 0 );
        cameraDolly.localPosition = new Vector3( 0, 0, -dist );
        cam.farClipPlane = zoomDistances.y * 1.1f;
    }

    void OnValidate() {
        FixedUpdate();
    }

    public void ReadPanInput(Vector3 deltaPos) {
        _S.poi = null;
        _S.targetPos = transform.position + deltaPos * panSpeed;
    }

    public void ReadZoomInput(float newZoomLerp) {
        _S.zoomLerp = newZoomLerp;
    }

    public void ReadRotateInput(bool rotatingRight) {
        if (rotatingRight) {
            _S.rotationY += rotateSpeed;
        }
        else {
            _S.rotationY -= rotateSpeed;
        }
    }

    private float toZoomLerp, fromZoomLerp, zoomEasingStart;
    private bool  easingZoom = false;

    public bool GetEasingZoom(){
        return _S.easingZoom;
    }
    
    public void EaseToZoomLerp( float tToZoomLerp ) {
        // if ( easingZoom ) { return; }
        fromZoomLerp = zoomLerp;
        toZoomLerp = tToZoomLerp;
        zoomEasingStart = Time.time;
        easingZoom = true;
    }

    static public void EASE_TO_ZOOM_LERP( float tToZoomLerp ) {
        _S.EaseToZoomLerp( tToZoomLerp );
    }

    static public void FOCUS_SHIP( CaptainRec cap ){
        if(cap.currentShip != null) _S.poi = cap.currentShip.transform;
    }
    static public float CAM_HEIGHT => ( _S == null ) ? 0 : _S.transform.position.y;
    static public float ZOOM_LERP  => ( _S == null ) ? 0 : _S.zoomLerp;
}

