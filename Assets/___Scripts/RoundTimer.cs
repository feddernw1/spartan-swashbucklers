using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent( typeof(TMP_Text) )]
public class RoundTimer : MonoBehaviour {
    private TMP_Text tmpText;
    
    private void Awake() {
        tmpText = GetComponent<TMP_Text>();
    }

    void Update() {
        System.TimeSpan span = TimeSpan.FromSeconds( Time.time );
        // tmpText.text = span.ToString( $"{span:hh}\\:{span:mm}\\:{span:ss}" );
        tmpText.text = span.ToString( "hh\\:mm\\:ss" );
    }
}
