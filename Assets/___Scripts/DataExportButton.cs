using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using XnTools;

// ScreenCapture Documentation:
// https://docs.unity3d.com/ScriptReference/ScreenCapture.CaptureScreenshot.html

public class DataExportButton : MonoBehaviour {
    static private DataExportButton _S;

    public TMP_Text dataTMP_Text;
    public string   fileNamePrefix = DEFAULT_PREFIX;

    private void Awake() {
        _S = this;
    }

    [Button]
    public void ClickToExportData() {
        if ( _S == null ) _S = this;
        ExportData( fileNamePrefix );
    }

    private const string DEFAULT_PREFIX = "Data Export";
    
    static public void ExportData( string fileNamePrefix = DEFAULT_PREFIX ) {
        // Check for a Singleton to override the defaults. - JGB 2024-04-19
        string dataString = "";
        if ( _S == null ) {
            Debug.LogError($"Cannot export data because there is no DataExportButton Singleton.");
            return;
        } else if ( _S.dataTMP_Text == null ) {
            Debug.LogError($"Cannot export data because the DataExportButton Singleton has no dataTMP_Text assigned.");
            return;
        } else {
            if ( fileNamePrefix == DEFAULT_PREFIX ) {
                fileNamePrefix = _S.fileNamePrefix;
            }
            dataString = _S.dataTMP_Text.text;
        }

        string filePath = "";
#if UNITY_EDITOR
        filePath = Application.persistentDataPath;
#else
        filePath = Application.persistentDataPath;
#endif
        
        System.TimeSpan span = TimeSpan.FromSeconds( Time.time );
        // tmpText.text = span.ToString( $"{span:hh}\\:{span:mm}\\:{span:ss}" );
        string spanString = $"{span:hh}h{span:mm}m{span:ss}s";
            
        
        string fileName = $"{fileNamePrefix}-{System.DateTime.Now.ToDateTimeStamp()}-{spanString}.txt";
        
        System.IO.File.WriteAllText( System.IO.Path.Combine( filePath, fileName ), dataString );
        
#if UNITY_EDITOR
        Debug.LogWarning( $"Saved \"{fileName}\" "
                          + " to the Persistent Data Path!\n"
                          + filePath );
#else
        Debug.LogWarning( $"Saved \"{fileName}\" "
                   + $" to path: \"{filePath}\" !" );
#endif
        
        if (_S != null) _S.SendXnBugReport( dataString );
    }
    
    void SendXnBugReport( string dataString ) {
        WWWForm wForm = new WWWForm();
        wForm.AddField( "userName", "Data Export" );
        wForm.AddField( "reportType", "Data Export" );
        wForm.AddField( "UDID", XnUniqueDeviceID.GetUDID() );
        wForm.AddField( "version", Application.version );
        wForm.AddField( "runData", dataString );
        
        System.TimeSpan span = TimeSpan.FromSeconds( Time.time );
        // tmpText.text = span.ToString( $"{span:hh}\\:{span:mm}\\:{span:ss}" );
        string spanString = $"{span:hh}:{span:mm}:{span:ss}";
        wForm.AddField( "duration", spanString );

        XnBugReporter.POST(wForm, SubmitFeedbackCallback);

        StartCoroutine( ShowModalPanelCoRo() );
    }

    IEnumerator ShowModalPanelCoRo() {
        // This was added because the Modal Panel was showing up in screenshots - JGB 2024-04-19
        yield return new WaitForSecondsRealtime( 0.1f );
        XnModalPanel.SHOW("Sending Data Export to the server...",null,""); // "" is third argument to not show any buttons.
    }

    void SubmitFeedbackCallback(bool success, string note) {
        if (success) {
            //inComment.text = $"Success!\n\nThank you for submitting your comment.\n\n{note}";
            XnModalPanel.SHOW($"Success!\n\nThank you for submitting your Data Export.");
        } else {
            //inComment.text = $"I'm sorry, but the comment didn't make it to our servers. Please submit it again later.\n\n{note}";
            XnModalPanel.SHOW($"I'm sorry, but the Data Export didn't make it to our servers. Please submit it again later.");
        }
    }
}

